package utils;

import models.TransactionModel;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Marcus on 2/11/2017.
 */
public class ReadFile {

    ArrayList<TransactionModel> transactions;

    public ArrayList<TransactionModel> readFile() {
        InputStream inp = null;
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("workbook.xlsx").getFile());
        DataFormatter dataFormatter = new DataFormatter();
        try {
            inp = new FileInputStream(file);
            Workbook wb = WorkbookFactory.create(inp);
            transactions = new ArrayList<TransactionModel>();

            for (Sheet sheet : wb) {
                for (Row row : sheet) {
                    if (row.getRowNum() == 0) {
                        continue;
                    }
                    Cell cardCell = row.getCell(3);
                    Cell debitCell = row.getCell(6);
                    Cell creditCell = row.getCell(7);
                    String debitStr = dataFormatter.formatCellValue(debitCell);
                    String creditStr = dataFormatter.formatCellValue(creditCell);
                    Double debit;
                    Double credit;

                    if (debitStr.equals("")){
                        debit = null;
                    } else {
                        debit = debitCell.getNumericCellValue();
                    }

                    if (creditStr.equals("")){
                        credit = null;
                    } else {
                        credit = creditCell.getNumericCellValue();
                    }
                    String cardString = dataFormatter.formatCellValue(cardCell);
                    TransactionModel transactionModel = new TransactionModel(
                            row.getCell(0).getStringCellValue(),
                            row.getCell(1).getDateCellValue(),
                            row.getCell(2).getDateCellValue(),
                            cardString,
                            row.getCell(4).getStringCellValue(),
                            row.getCell(5).getStringCellValue(),
                            debit,
                            credit);
                    transactions.add(transactionModel);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return transactions;
    }

}
