import models.TransactionModel;
import utils.ReadFile;

import java.util.ArrayList;

/**
 * Created by Marcus on 2/11/2017. :)
 */
public class ExpensesMain {

    static final double BUDGET = 2924;
    static final double CPS = 130;
    static final double TWC = 130;
    static final double SAWS = 70;
    static final double MORTGAGE = 840;
    static final double LAWN_SERVICES = 108;

    public static void main(String[] args) {
        ReadFile readFile = new ReadFile();
        ArrayList<TransactionModel> transactions = readFile.readFile();
        ArrayList<TransactionModel> merchandise = new ArrayList<TransactionModel>();
        ArrayList<TransactionModel> dining = new ArrayList<TransactionModel>();
        ArrayList<TransactionModel> gas = new ArrayList<TransactionModel>();

        for (TransactionModel transaction : transactions){
            if(transaction.getCategory().equals("Merchandise")){
                merchandise.add(transaction);
            } else if (transaction.getCategory().equals("Dining")){
                dining.add(transaction);
            } else if (transaction.getCategory().contains("Gas")){
                gas.add(transaction);
            }
        }

        double totalMerchandise = 0;

        System.out.println("--------------------------Merchandise--------------------------");
        for(TransactionModel transaction : merchandise){
            System.out.println(transaction);
            if (transaction.getDebit() != null){
                totalMerchandise+=transaction.getDebit();
            }

        }

        System.out.println("-----------------------------Dining-----------------------------");
        double totalDining = 0;
        for(TransactionModel transaction : dining){
            System.out.println(transaction);
            if (transaction.getDebit() != null){
                totalDining+=transaction.getDebit();
            }

        }

        System.out.println("-------------------------------Gas-------------------------------");
        double totalGas = 0;
        for(TransactionModel transaction : gas){
            System.out.println(transaction);
            if (transaction.getDebit() != null){
                totalGas+=transaction.getDebit();
            }

        }

        System.out.println("----------------------------------------------------------------");
        System.out.println("Budget: "+BUDGET);
        System.out.println("Mortgage: "+MORTGAGE);
        System.out.println("CPS: "+CPS);
        System.out.println("TWC: "+TWC);
        System.out.println("SAWS: "+SAWS);
        System.out.println("Lawn Services: "+LAWN_SERVICES);
        System.out.println("Gas: "+totalGas);
        double totalFixed = (CPS + TWC + SAWS + MORTGAGE + LAWN_SERVICES + totalGas);
        System.out.println("Total fixed expenses: "+ totalFixed);
        System.out.println();
        System.out.println("Total Merchandise (Excluding Amazon Store Card): "+totalMerchandise);
        System.out.println("Total Dining: "+totalDining);
        System.out.println("Total Merch+Dining: "+(totalMerchandise+totalDining));
        System.out.println();
        System.out.println("Net Income: "+ (BUDGET - totalFixed - totalDining - totalMerchandise));

    }

}
