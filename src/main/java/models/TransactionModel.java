package models;

import java.util.Date;

/**
 * Created by Marcus on 2/11/2017.
 */
public class TransactionModel {
    private String stage;
    private Date transactionDate;
    private Date postedDate;
    private String cardNum;
    private String description;
    private String category;
    private Double debit;
    private Double credit;

    public TransactionModel(String stage, Date transactionDate, Date postedDate, String cardNum,
                            String description, String category, Double debit, Double credit) {
        this.stage = stage;
        this.transactionDate = transactionDate;
        this.postedDate = postedDate;
        this.cardNum = cardNum;
        this.description = description;
        this.category = category;
        this.debit = debit;
        this.credit = credit;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(Date postedDate) {
        this.postedDate = postedDate;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    @Override
    public String toString() {
        return "TransactionModel{" +
                "stage='" + stage + '\'' +
                ", transactionDate=" + transactionDate +
                ", postedDate=" + postedDate +
                ", cardNum='" + cardNum + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                ", debit=" + debit +
                ", credit=" + credit +
                '}';
    }
}
